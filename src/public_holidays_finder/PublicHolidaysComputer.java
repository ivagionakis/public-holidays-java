package public_holidays_finder;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.TemporalAdjusters.next;
import static java.time.temporal.TemporalAdjusters.firstInMonth;

public class PublicHolidaysComputer {
    private HashSet<Integer> holidays = new HashSet<>();
    private int easter;
    private final String[] knowndates= {"1 Ιανουαρίου", "6 Ιανουαρίου", "25 Μαρτίου", "1 Μαΐου", "15 Αυγούστου", "28 Οκτωβρίου",
            "17 Νοεμβρίου", "25 Δεκεμβρίου", "26 Δεκεμβρίου"};
    private final int[] aroundEaster = {-48, -2, 1, 50};
    private int year;

    public PublicHolidaysComputer(){}
//    EortologioScrapper es = new EortologioScrapper();

    public Set findAll(int year) throws IOException {
//        this.findAllWeekendsInYear(year);
        //easter = es.findDesiredHoliday(year, "Άγιο Πάσχα", this);
        this.year = year;
        easter = findEaster();
        holidays.add(easter);
        findHolidaysAroundEaster();
        findDatesDayOfYear();
        System.out.println(dayOfYearToLocalDate());
        return holidays;
    }

    public void findHolidaysAroundEaster(){
        for (int day: aroundEaster){
            holidays.add(easter+day);
        }
    }

    public void findDatesDayOfYear(){
        for (String date: knowndates){
            LocalDate ld = findLocalDate(date+ " " + String.valueOf(year));
            holidays.add(ld.getDayOfYear());
        }
    }

    public int findEaster(){
        int e = year - 2;
        e %= 19;
        e *= 11;
        e %= 30;
        int julianMonth = 0;
        int diff = 44 - e;
        if (e > 23){
            julianMonth = 4;
        }else{
            julianMonth = 3;
        }
        if ((((diff > 31)  && (julianMonth == 3))) || (((diff > 30) && (julianMonth == 4)))){
            diff -= 31 + (3 - julianMonth);
            julianMonth += 1;
        }
        String date = "";
        switch (julianMonth){
            case 3:
                date += diff+ " " + "Μαρτίου " + year;
                break;
            case 4:
                date += diff+ " " + "Απριλίου " + year;
                break;
            case 5:
                date += diff+ " " + "Μαΐου " + year;
                break;
        }
        LocalDate ld = findLocalDate(date);
        ld = ld.plus(Period.ofDays(13));
        LocalDate easterd = ld.with(next(SUNDAY));
//        System.out.println(easterd);
        return easterd.getDayOfYear();
    }

    public void findAllWeekendsInYear(){
        LocalDate now = LocalDate.of(year, Month.JANUARY, 1);
        LocalDate sunday = now.with(firstInMonth(DayOfWeek.SUNDAY));
        holidays.add(sunday.getDayOfYear());
        LocalDate saturday = now.with(firstInMonth(DayOfWeek.SATURDAY));
        holidays.add(saturday.getDayOfYear());
        do {
            saturday = saturday.plus(Period.ofDays(7));
            sunday = sunday.plus(Period.ofDays(7));
            if (saturday.getYear() == year) {
                holidays.add(saturday.getDayOfYear());
            }
            if (sunday.getYear() == year) {
                holidays.add(sunday.getDayOfYear());
            }
        } while (sunday.getYear() == year || saturday.getYear() == year);
    }

    public LocalDate findLocalDate(String date){
        char[] i = new char[2];
        date.getChars(0, 2, i, 0);
        if (i[1] == (" ").charAt(0)){
            date = "0"+date;
        }
        Locale locale = new Locale("el", "GR");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy").withLocale( locale );
        LocalDate sDate = formatter.parse(date, LocalDate :: from);
        return sDate;
    }

    public Set<LocalDate> dayOfYearToLocalDate(){
        Set ldholidays = new TreeSet();
        for (int doy: holidays){
            LocalDate ld = LocalDate.ofYearDay(year, doy);
            ldholidays.add(ld);
        }
        return(ldholidays);
    }
}

