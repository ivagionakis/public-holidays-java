package public_holidays_finder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class EortologioScrapper {
    private Document doc;

    public int findDesiredHoliday(int year, String holiday, PublicHolidaysComputer phc) throws IOException {
        String syear = String.valueOf(year);

        doc = Jsoup.connect("https://www.eortologio.net/argies/year/"+ syear).get();
        Elements elements = doc.getElementsByAttributeValueMatching("id", "^\\d( ?\\d){0,20}$");
        Element desired = new Element("de");
        for (Element element : elements) {
            for (Element child : element.children()) {
                if (child.ownText().equals(holiday)) {
                    desired = element;
                    break;
                }
            }
            if (!desired.tagName().equals("de")) {
                break;
            }
        }
        return phc.findLocalDate(desired.child(0).ownText() + " " + syear).getDayOfYear();
    }
}