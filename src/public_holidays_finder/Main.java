package public_holidays_finder;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) throws IOException {
        PublicHolidaysComputer phc = new PublicHolidaysComputer();
        Set holidays = phc.findAll(Integer.valueOf(args[0]));
        Set sorted = new TreeSet(holidays);
        System.out.println(sorted);
    }

}
